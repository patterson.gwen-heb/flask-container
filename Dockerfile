FROM python:3.7.4-alpine3.10
WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
ENV FLASK_APP=server.py
EXPOSE 5000
CMD ["flask", "run", "--host", "0.0.0.0"]