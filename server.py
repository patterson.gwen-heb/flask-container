from flask import Flask, escape, request

app = Flask(__name__)

@app.route('/')
def hello():
    name = request.args.get("name", "World")
    return f'Hello, {escape(name)} now with signed commits back to vscode'

if __name__ == "__main__":
    app.run("0.0.0.0", port=80, debug=True)